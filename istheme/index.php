<?php
/**
 * The main template file
 *
 */
?>
<div id="wrapper">
	<div id="main">
		<?php get_header(); ?>
		<div id="content">
		<?php if ( have_posts() ) : ?>

			<?php /* The loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>
				<div class="post clearfix">
					<div class="thumb">
						<?php
							if ( has_post_thumbnail() ) {
								the_post_thumbnail();
							} 
						?>
					</div>
					<div class="title"><?php the_title(); ?></div>
				</div>
			<?php endwhile; ?>

			<div class="pagination">
			    <?php
			     echo paginate_links(array(  
			      'prev_text' => 'Previous page',  
			      'next_text' => 'Next page',
			     )); 
	    		?>
    		</div>

		<?php else : ?>
			<p>No posts were found!</p>
		<?php endif; ?>

		</div><!-- #content -->
		<?php get_footer();?>
	</div>
</div>