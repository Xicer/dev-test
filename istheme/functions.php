<?php

add_theme_support( 'post-thumbnails' );

function be_event_query( $query ) {
	
	if( $query->is_main_query() && !is_admin() ) {
		$meta_query = array(
			array(
				'key' => 'rating'
			)
		);
		$query->set( 'meta_query', $meta_query );
		$query->set( 'orderby', 'meta_value_num' );
		$query->set( 'meta_key', 'rating' );
		$query->set( 'order', 'DESC' );
		$query->set( 'posts_per_page', '2' );
	}

}

add_action( 'pre_get_posts', 'be_event_query' );

?>