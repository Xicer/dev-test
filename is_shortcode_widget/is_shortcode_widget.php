<?php
/*
Plugin Name: Shortcode and widget generator
Description: The plugin generates few shortcodes and a calculator widget
Version: 1.0
Author: Ivica Sertić
Author URI: ivica-sertic.from.hr
*/


///// Array with shortcode configs ////////////////////////////////
// name = name string | name of shortcode tag
// html_tag = string | tag to use while generating the shortcode
// content = bool | true if shortcode has content and closing tag
///////////////////////////////////////////////////////////////////

$shortCodes =[ 
	['name' => 'is_image', 'html_tag' => 'img', 'content' => false], 
	['name' => 'is_audio', 'html_tag' => 'audio', 'content' => true],
	['name' => 'is_source', 'html_tag' => 'source', 'content' => false],
	['name' => 'is_article', 'html_tag' => 'article', 'content' => true], 
	['name' => 'is_video', 'html_tag' => 'video', 'content' => true]
];

// build shortcodes
foreach ($shortCodes as &$shortCode) {
	new buildShortCode($shortCode);
} 

// Shortcode builder
class buildShortCode {

	public $name;
	public $html_tag = 'p'; //default tag
	public $content = false;

	public function __construct( array $cfg){
       $this->name = $cfg['name'];
       $this->html_tag = $cfg['html_tag'];
       $this->content = $cfg['content'];
       $this->registerShortCode();
    }

    public function registerShortCode() {
    	if (isset($this->name)) {
    		if ($this->content) {
    			$handler = function ($atts, $content = null) {
    				$att = '';
    				if (!empty($atts)) {
    					foreach ($atts as $key => $value) {
    						$att .= ' '.$key.'="'.$value.'"';
    					}
    				}
    				return '<'.$this->html_tag.$att.'>'.do_shortcode($content).'</'.$this->html_tag.'>';
    			};
		   	} else {
		   		$handler = function ($atts) {
		   			$att = '';
		   			if (!empty($atts)) {
		   				foreach ($atts as $key => $value) {
		   					$att .= ' '.$key.'="'.$value.'"';
		   				}
		   			}

		   			return '<'.$this->html_tag.$att.'>';
		    	};
		   	}

	    	add_shortcode($this->name, $handler);
	    }
    }
}

// Calculator widget
class isCalculator extends WP_Widget {

	function isCalculator() {
        parent::WP_Widget(false, $name = __('iSCalculator', 'wp_widget_plugin') );
    }

    // widget form
	function form($instance) {
		if ($instance) {
		    $title = esc_attr($instance['title']);
		} else {
		    $title = '';
		}

		require_once(plugin_dir_path( __FILE__ ).'/views/calculator/form/layout.html');
	}

	// widdget settings update
	function update($new_instance, $old_instance) {
		$instance = $old_instance;
	    $instance['title'] = strip_tags($new_instance['title']);
	    
	    return $instance;
	}

	// widget display
	function widget($args, $instance) {
		$title = $instance['title'];	
		require_once(plugin_dir_path( __FILE__ ).'/views/calculator/widget/layout.html');
	}
}

// register widget
add_action('widgets_init', create_function('', 'return register_widget("isCalculator");'));

// register assets
function widget_scripts() {
	wp_enqueue_script('is-calculator-js', plugins_url( 'assets/js/isCalculator.js', __FILE__ ), array(), '1.0.0', true );
	wp_register_style( 'is-calculator-style', plugins_url( 'assets/css/style.css', __FILE__ ));
}

add_action('wp_enqueue_scripts', 'widget_scripts');


?>