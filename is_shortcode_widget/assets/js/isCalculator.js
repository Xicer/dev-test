//get all keys
var keys = document.querySelectorAll('#is-calc .num-pad span');
//define operations
var operations = ['+', '-', '*', '/'];

//add onclick event for each key
for(var i = 0; i < keys.length; i++) {
	keys[i].onclick = function(e) {
		//input selector
		var input = document.querySelector('.display');
		//input value from .display
		var inputVal = input.innerHTML;
		//button value
		var btnVal = this.innerHTML;
		
		//if operation is clicked
		if(operations.indexOf(btnVal) > -1) {
			//get the last character from the input
			var lastChar = inputVal[inputVal.length - 1];
			
			//if the input is not empty and if there is no operation as last character add the operation
			if(inputVal != '' && operations.indexOf(lastChar) == -1) {
				input.innerHTML += btnVal;
			//allow - only if the input is empty	
		    } else if(inputVal == '' && btnVal == '-') {
				input.innerHTML += btnVal;
			}
			
			//if an operation was the last char, replace it with the new operation
			if(operations.indexOf(lastChar) > -1 && inputVal.length > 1) {
				input.innerHTML = inputVal.replace(/.$/, btnVal);
			}

		//if equal is clicked
		} else if(btnVal == '=') {
			//get the equation
			var equation = inputVal;
			//get the last character in the equation
			var lastChar = equation[equation.length - 1];

			//if last character is an operator or decimal, remove it
			if(operations.indexOf(lastChar) > -1 || lastChar == '.') {
				equation = equation.replace(/.$/, '');
			}

			if(equation) {
				input.innerHTML = eval(equation);
			}

		//if clear is clicked
		} else if (btnVal == 'C') {
			input.innerHTML = '';
		} else {
			//append to display
			input.innerHTML += btnVal;
		}
	} 
}